import React, { Component } from 'react';
import SignUp from "./SignUp"
import App from "./App"
import {BrowserRouter as Router, Route, Switch , withRouter} from 'react-router-dom';

class Initial extends Component{
    constructor(props){
        super(props)
        this.state = {

        }
    }

    render(){
        return(
            <div>
                
                    <Route path="/SignUp" component={SignUp} />
                    <Route path="/App2" component={App} />
                    <Route path="/" component={App} />
                
            </div>
        )
    }
}

export default withRouter(Initial)