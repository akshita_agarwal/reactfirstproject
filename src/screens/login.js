import React, { Component } from 'react';
import './SignUp.css';
import { withRouter } from 'react-router';
import FontAwesome from 'react-fontawesome';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'

class SignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            password: "",
            email: "",
            lastName: "",
            nameError: false,
            emailError: false,
            country: '',
            lastNameError: "",
            passwordError: false,
            emailErrorMsg: "",
            showPassword:false,
            passwordErrorMsg : false
        }

    }

    render() {
        const { nameError, showPassword ,lastNameError, emailErrorMsg, passwordError, emailError, password,passwordErrorMsg } = this.state
        return (
            <form className="SignIn" onSubmit={this.handleOnSubmit}>
                <div className="Container" >

                    <div className="ChildLeftContainer" >
                    </div>
                    
                    <div className="ChildRightContainer" >
                        <label className="LabelStyle">Sign In</label>
                        <label className="textStyle">Please enter your details to sign up .</label>
                        
                        <div className="innerContainer" >
                            <label className="fieldTextStyle">Name:</label><br />
                            <div className="textInputView" >
                                <input
                                    className={(nameError) ? "errorTextFieldStyle" : "textFieldStyle"}
                                    type="hidden"
                                    name="name"
                                    type="text"
                                    placeholder="First name"
                                    onChange={this.handleName}
                                >
                                </input>
                            </div>
                            <br />
                            {
                                nameError ?
                                    <label className="errorTextStyle">Please enter name</label> : null
                            }
                        </div>

                        <div className="innerContainer">
                            <label className="fieldTextStyle">Last Name:</label><br />
                            <div className="textInputView" >
                                <input
                                    className={(lastNameError) ? "errorTextFieldStyle" : "textFieldStyle"}
                                    name="Last Name"
                                    type="text"
                                    placeholder="Last name"
                                    onChange={this.handleLastName}
                                >
                                </input>
                            </div>
                            <br />
                            {
                                lastNameError ?
                                 <label className="errorTextStyle">Please enter last name</label> : null
                            }

                        </div>

                        <div className="innerContainer">
                            <label className="fieldTextStyle">Password:</label><br />
                            <div className="textInputView">
                                <input
                                    className={(passwordError) ? "errorTextFieldStyle" : "textFieldStyle"}
                                    name="Password"
                                    placeholder="Password"
                                    type={showPassword?"text":"password"}
                                    value = {password}
                                    maxLength = {8}
                                    onChange={this.handlePassword}
                                >
                                </input>
                                <span onClick={()=>this.setState({showPassword:!showPassword})}>
                                    <FontAwesomeIcon className = "eyeIconStyle" icon={showPassword?faEye:faEye}/>
                                </span>
                            </div>
                            <br />
                            {
                                passwordError ?
                                    <label className="errorTextStyle">{passwordErrorMsg}</label> : null
                            }

                        </div>
                        <div className="innerContainer">
                            <label className="fieldTextStyle">Email:</label><br />
                            <div className="textInputView">
                                <input
                                    className={(emailError) ? "errorTextFieldStyle" : "textFieldStyle"}
                                    name="Email"
                                    type="email"
                                    placeholder="Email"
                                    onChange={this.handleEmail}
                                >
                                </input>
                            </div>
                            <br />
                            {
                                emailError ?
                                    <label className="errorTextStyle" >{emailErrorMsg}</label> : null
                            }

                        </div>
                        <div className = "buttonContainerView">
                            <input className = "buttonStyle" type="submit" value="REGISTER"  onClick={this.handleClick} />
                        </div>
                    </div>
                </div>
            </form>
        )
    }


    handleName = (event) => {

        this.setState({ name: event.target.value })


    }
    handleLastName = (event) => {

        this.setState({ lastName: event.target.value, })


    }
    handlePassword = (event) => {

        this.setState({ password: event.target.value, })

    }

    handleEmail = (event) => {
        this.setState({ email: event.target.value, })
    }

    checkValidation = () => {
        const { name, password, lastName, emailError, email, nameError,passwordError,lastNameError } = this.state
      
        if (name == "") {
            this.setState({ nameError: true })
            return false
        } else {
            this.setState({ nameError: false })
        }
        if (lastName == "") {
            this.setState({ lastNameError: true })
            return false
        } else {
            this.setState({ lastNameError: false })
        }
        if (password == "") {
            this.setState({ passwordError: true,passwordErrorMsg : "Please enter password." })
            return false
        } else {
            this.setState({ passwordError: false })
        }
        if (password != "") {
            if(password.length < 6){
                this.setState({ passwordError: true,passwordErrorMsg : "Password must contain at least 6 character" })
                return false
            }
            
        } else {
            this.setState({ passwordError: false })
        }
        if (email == "") {
            this.setState({ emailError: true, emailErrorMsg: "Please enter Email" })
            return false
        } else {
            this.setState({ emailError: false })
        }
        if (email != "") {
            if (!(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email))) {
                this.setState({ emailError: true, emailErrorMsg: "Please enter correct Email" })
                return false
            } else {
                this.setState({ emailError: false })
        
            }
        }

        if(nameError == false && emailError == false && passwordError == false && lastNameError == false){
            return true
        }

    }
    selectCountry = (val) => {
        this.setState({ country: val });
    }

    handleClick = (e) => {
        e.preventDefault();
        if(this.checkValidation()){
            this.props.history.push("/App2");
        }
    } 

}

export default SignIn
